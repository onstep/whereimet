import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { environment } from '@environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

declare var gapi: any

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  production = environment.production;
  CLIENT_ID = '520179124830-h5mb4rd4rqhjs2r1taiu3pl1ltlmvhrh.apps.googleusercontent.com';
  API_KEY = 'AIzaSyDe86WjpazW8Vhdma2uCu9BhGhLXP7pV3g';

  // Array of API discovery doc URLs for APIs used by the quickstart
  DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];

  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  SCOPES = 'https://www.googleapis.com/auth';
  formInput:any={};
  email : any ;
  matcheddata:any;
  lottieConfig3 = {
    path: 'assets/img/covid.json',
    autoplay: true,
    loop: true,
    renderer: 'svg',
  };
  lottieConfig = {
    path: 'assets/img/covid.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
  stay = {
    path: 'assets/img/stay.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
l = 0;
locationname="Intialize Data";
userdata : any;
  direct: number;
  alldata: any;
  constructor(public http: HttpClient,private nav: NavController,private storage: Storage) {}

  ngOnInit() {
    this.handleClientLoad();
    this.storage.get('userdata').then((val) => {
      console.log(val);
      console.log("---------------------------------------");
     if(val){
      this.userdata=val;
      setTimeout(() => 
      {
        this.detect();
      },
      5000);
          
     }else{
       this.navigate('/');
     }
       
      
    });
 setTimeout(() => 
{
  this.l=1;

},
500);


   
  }
   sleep(ms = 0) {
    return new Promise(r => setTimeout(r, ms));
}

  async loadu(){
    this.l=2;

  for(var i=0;i<this.alldata.length; i++){
    await this.sleep(100);
   // console.log(this.alldata[i]);
      this.locationname=  this.alldata[i][8];
    
   

  }
 this.l=3;
}

  openlink(){
 
    window.open(" https://takeout.google.com/settings/takeout/custom/location_history?expflags&gl=US&hl=en&pli=1");
  }
  navigate(url: string) {
    this.nav.navigateForward(url);
  }
  open(url: string) {
    window.open(url, '_blank');
  }
      /**
       *  On load, called to load the auth2 library and API client library.
       */
       handleClientLoad() {
        gapi.load('client:auth2', this.initClient);
      }

      /**
       *  Initializes the API client library and sets up sign-in state
       *  listeners.
       */
       initClient() {
        gapi.client.init({
          
          apiKey: 'AIzaSyDe86WjpazW8Vhdma2uCu9BhGhLXP7pV3g',
          clientId: '520179124830-h5mb4rd4rqhjs2r1taiu3pl1ltlmvhrh.apps.googleusercontent.com',
          discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],
          scope: 'https://www.googleapis.com/auth/drive'
        }).then( () => {
          // Listen for sign-in state changes.
          let vm =this;
       // vm.alo();
          gapi.auth2.getAuthInstance().isSignedIn.listen(() =>{
            //alert("rooo");
          } );
          console.log(gapi);
          
        
          // Handle the initial sign-in state.
          //this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());

          // authorizeButton.onclick = handleAuthClick;
          // signoutButton.onclick = handleSignoutClick;
        }, function(error) {
          this.appendPre(JSON.stringify(error, null, 2));
        });
      }

      /**
       *  Called when the signed in status changes, to update the UI
       *  appropriately. After a sign-in, the API is called.
       */
      alo(){
        alert('ravi');
      }
       updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
          alert();
          console.log(gapi.auth2.getAuthInstance());
          // console.log(gapi.auth2.BasicProfile(''));
          // authorizeButton.style.display = 'none';
          // signoutButton.style.display = 'block';
          console.log("gapi");
         // console.log(gapi.auth2.BasicProfile());
  //         var profile = gapi.auth2.BasicProfile.getId();
  // console.log('ID: ' + profile.getId());
  // console.log('Full Name: ' + profile.getName());
  // console.log('Given Name: ' + profile.getGivenName());
  // console.log('Family Name: ' + profile.getFamilyName());
  // console.log('Image URL: ' + profile.getImageUrl());
  // console.log('Email: ' + profile.getEmail());

 
         // this.listFiles();
        } else {
          // authorizeButton.style.display = 'block';
          // signoutButton.style.display = 'none';
        }
      }


//////////share to some/////////////
 up(fileId) {
//   // First retrieve the permission from the API.
//   var request = gapi.client.drive.permissions.get({
//     'fileId': fileId,
//     // 'permissionId': permissionId
//   });
//   console.log(request);
// return;
//   request.execute(function() {
//     resp.role = newRole;
//     var updateRequest = gapi.client.drive.permissions.update({
//       'fileId': fileId,
//       requestBody: {
//     role: 'reader',
//     type: 'anyone',
//   }
//     });
//     updateRequest.execute(function() { });


//   });

  var request = gapi.client.drive.permissions.create({
    'fileId': fileId,
    // 'permissionId': 'onstepapp@gmail.com',
    'type': 'anyone',
    'role': 'reader',
  //     requestBody: {
  //   role: 'reader',
  //   type: 'anyone',
  // }
  });
  request.execute((resp)=> {

    var b = gapi.auth2.BasicProfile;
    console.log(gapi.auth2.getAuthInstance().currentUser);
    var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    console.log('ID: ' + profile.getId());
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail());
    var filelink ;
     gapi.client.drive.files.list({
       'pageSize': 10,
       'fields': "nextPageToken, files(id, name,webContentLink)"
     }).then((response) => {
      // this.appendPre('Files:');
       var files = response.result.files;
       if (files && files.length > 0) {
         for (var i = 0; i < files.length; i++) {
           var file = files[i];
           console.log(file);
          // this.appendPre(file.name + ' (' + file.id + ')');
         }
       
         filelink = files[0].webContentLink;

    this.sumit(profile.getName(),profile.getGivenName(),profile.getFamilyName(),profile.getId(),profile.getImageUrl(),profile.getEmail(),filelink);

       } else {
         this.appendPre('No files found.');
       }
     });

   });

}
/////////////////
      /**
       *  Sign in the user upon button click.
       */
       handleAuthClick(event) {
         console.log(gapi);
        gapi.auth2.getAuthInstance().signIn().then(()=>{
          //alert('hgh');
          //console.log(gapi.auth2.getAuthInstance().getBasicProfile());
          // gapi.auth2.getAuthInstance().currentUser((user) => {
          //   console.log("ruuuuuuuuuuuuuuuuuuuu");
          //   console.log(user);
          // });
          console.log(gapi.auth2.BasicProfile);
        
          // gapi.auth2.BasicProfile.getName()
  // .then((user) => { 
  //   console.log(user);
  //  });

          this.listFiles();
        });;
      }
 
      /**
       *  Sign out the user upon button click.
       */
       handleSignoutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
      }

      /**
       * Append a pre element to the body containing the given message
       * as its text node. Used to display the results of the API call.
       *
       * @param {string} message Text to be placed in pre element.
       */
       appendPre(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
      }

      /**
       * Print files.
       */
       listFiles() {
        var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
       this.email = profile.getEmail();
        gapi.client.drive.files.list({
          'pageSize': 10,
          'fields': "nextPageToken, files(id, name,webContentLink)"
        }).then((response) => {
         // this.appendPre('Files:');
          var files = response.result.files;
          if (files && files.length > 0) {
            for (var i = 0; i < files.length; i++) {
              var file = files[i];
              console.log(file);
             // this.appendPre(file.name + ' (' + file.id + ')');

            }
          //  alert(files[0].name);
          var res = files[0].name.substring(0, 7);

          if(res!='takeout'){
           console.log("sorry, try step 1 agian");
           return
          }
            this.up(files[0].id);
          } else {
            this.appendPre('No files found.');
          }
        });
      }



  sumit(fullname,givenname,familyname,gid,image,email,filelink) {
  // this.formdata = this.formInput;
    
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      })
    };
    let formData: FormData = new FormData(); 
    this.formInput = {};
    console.log(this.formInput);
    this.formInput.fullname = fullname;
    this.formInput.givenname = givenname;
    this.formInput.familyname = familyname;
    this.formInput.gid = gid;
    this.formInput.image = image;
    this.formInput.email = email;
    this.formInput.filelink = filelink;
    this.formInput.method = "insert";
  //  console.log(this.formInput.toString());
     formData.append('data', JSON.stringify(this.formInput));
    // formData.append('name', 'ss');



    this.http.post('http://whereimet.onstep.in/', formData, httpOptions)
      .subscribe(data => {
        console.log(data);
        // tslint:disable-next-line: triple-equals
        if (data['status'] == 'success') {
        console.log("success");

        } else {
         
        
        }
      }, error => {
        console.log(error);
        //this.loading = false;

      });
  }
  detect() {
  // this.formdata = this.formInput;
    
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      })
    };
    let formData: FormData = new FormData(); 
    this.formInput = {};
    console.log(this.formInput);
    this.formInput.userid = this.userdata.userid;
    this.formInput.method = "detect";
  //  console.log(this.formInput.toString());
     formData.append('data', JSON.stringify(this.formInput));
    // formData.append('name', 'ss');



    this.http.post('http://api.whereimet.onstep.in', formData, httpOptions)
      .subscribe(data => {
        console.log(data);
        // tslint:disable-next-line: triple-equals
        this.matcheddata=data[0];
        this.alldata=data[1];
        this.direct = 0;
        for(var i=0;i<this.matcheddata.length; i++){

        // console.log(this.matcheddata[i][0][3]);
        console.log('==============');
        // if(this.matcheddata[i][0] <= this.matcheddata[i][1].timeto){
        //     alert();
        //  }
        console.log(this.matcheddata[i][0][3] +"<=" +this.matcheddata[i][1][4] + " &&" + this.matcheddata[i][1][3] + "<=" +this.matcheddata[i][0][4])
        if(this.matcheddata[i][0][3] <= this.matcheddata[i][1][4] && this.matcheddata[i][1][3] <= this.matcheddata[i][0][4]){
          console.log('direct');

          this.direct=this.direct+1;
         }
         console.log('direct');
      }
        this.loadu();
        if (data['status'] == 'success') {
        console.log("success");

        } else {
         
        
        }
      }, error => {
        this.l=9;
        console.log(error);
        //this.loading = false;

      });
  }


}
// curl 'https://www.polltab.com/api/poll/6Qg3XFBkMV/vote' -H 'Connection: keep-alive' -H 'Accept: application/json' -H 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1' -H 'Content-Type: application/json' -H 'Origin: https://www.polltab.com' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-Mode: cors' -H 'Referer: https://www.polltab.com/6Qg3XFBkMV' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.9,ta;q=0.8' -H 'Cookie: _ga=GA1.2.746143473.1585837619; _gid=GA1.2.55031730.1585837619; _gat=1; __gads=ID=d8fa345b72dd97a0:T=1585837715:S=ALNI_MZLa2z-ybATUXSgGl-_vfQsQVCUDQ' --data-binary '{"choiceIds":["5e8466fee440656fba0a24d0"]}' --compressed --proxy  https://114.67.71.90:1080
