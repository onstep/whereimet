import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { environment } from '@environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { IonContent } from '@ionic/angular';

declare var gapi: any
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  @ViewChild(IonContent,{static:true}) content: IonContent;
  
  production = environment.production;
  CLIENT_ID = '520179124830-h5mb4rd4rqhjs2r1taiu3pl1ltlmvhrh.apps.googleusercontent.com';
  API_KEY = 'AIzaSyDe86WjpazW8Vhdma2uCu9BhGhLXP7pV3g';

  // Array of API discovery doc URLs for APIs used by the quickstart
  DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];

  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  SCOPES = 'https://www.googleapis.com/auth';
  formInput:any={};
  email : any ;
  matcheddata:any;
  lottieConfig3 = {
    path: 'assets/img/covid.json',
    autoplay: true,
    loop: true,
    renderer: 'svg',
  };
  lottieConfig = {
    path: 'assets/img/covid.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
userdata:any={};

  constructor(public http: HttpClient,private nav: NavController,private storage: Storage) {}

  ngOnInit() {
    this.handleClientLoad();
    this.storage.get('userdata').then((val) => {
      console.log('Your age is', val);
      if(val){
        this.navigate('detect');
      }
    });
  }
  scrollContent() {
   
    setTimeout(() => {
      console.log('Test');
      const elmnt = document.getElementById('how');
      elmnt.scrollIntoView();
  }, 100);
  
  }
  openlink(){
 
    window.open(" https://takeout.google.com/settings/takeout/custom/location_history?expflags&gl=US&hl=en&pli=1");
  }
  navigate(url: string) {
    this.nav.navigateForward(url);
  }
  open(url: string) {
    window.open(url, '_blank');
  }
      /**
       *  On load, called to load the auth2 library and API client library.
       */
       handleClientLoad() {
        gapi.load('client:auth2', this.initClient);
      }

      /**
       *  Initializes the API client library and sets up sign-in state
       *  listeners.
       */
       initClient() {
        gapi.client.init({
          
          apiKey: 'AIzaSyDe86WjpazW8Vhdma2uCu9BhGhLXP7pV3g',
          clientId: '520179124830-h5mb4rd4rqhjs2r1taiu3pl1ltlmvhrh.apps.googleusercontent.com',
          discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],
          scope: 'https://www.googleapis.com/auth/drive'
        }).then( () => {
          // Listen for sign-in state changes.
          let vm =this;
       // vm.alo();
          gapi.auth2.getAuthInstance().isSignedIn.listen(() =>{
            // alert("rooo");
          } );
          console.log(gapi);
          
        
          // Handle the initial sign-in state.
          //this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());

          // authorizeButton.onclick = handleAuthClick;
          // signoutButton.onclick = handleSignoutClick;
        }, function(error) {
          this.appendPre(JSON.stringify(error, null, 2));
        });
      }

      /**
       *  Called when the signed in status changes, to update the UI
       *  appropriately. After a sign-in, the API is called.
       */
      alo(){
        alert('ravi');
      }
       updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
          alert();
          console.log(gapi.auth2.getAuthInstance());
          // console.log(gapi.auth2.BasicProfile(''));
          // authorizeButton.style.display = 'none';
          // signoutButton.style.display = 'block';
          console.log("gapi");
         // console.log(gapi.auth2.BasicProfile());
  //         var profile = gapi.auth2.BasicProfile.getId();
  // console.log('ID: ' + profile.getId());
  // console.log('Full Name: ' + profile.getName());
  // console.log('Given Name: ' + profile.getGivenName());
  // console.log('Family Name: ' + profile.getFamilyName());
  // console.log('Image URL: ' + profile.getImageUrl());
  // console.log('Email: ' + profile.getEmail());

 
         // this.listFiles();
        } else {
          // authorizeButton.style.display = 'block';
          // signoutButton.style.display = 'none';
        }
      }


//////////share to some/////////////
 up(fileId) {
//   // First retrieve the permission from the API.
//   var request = gapi.client.drive.permissions.get({
//     'fileId': fileId,
//     // 'permissionId': permissionId
//   });
//   console.log(request);
// return;
//   request.execute(function() {
//     resp.role = newRole;
//     var updateRequest = gapi.client.drive.permissions.update({
//       'fileId': fileId,
//       requestBody: {
//     role: 'reader',
//     type: 'anyone',
//   }
//     });
//     updateRequest.execute(function() { });


//   });

  var request = gapi.client.drive.permissions.create({
    'fileId': fileId,
    // 'permissionId': 'onstepapp@gmail.com',
    'type': 'anyone',
    'role': 'reader',
  //     requestBody: {
  //   role: 'reader',
  //   type: 'anyone',
  // }
  });
  request.execute((resp)=> {

    var b = gapi.auth2.BasicProfile;
    console.log(gapi.auth2.getAuthInstance().currentUser);
    var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    console.log('ID: ' + profile.getId());
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail());
    var filelink ;
     gapi.client.drive.files.list({
       'pageSize': 10,
       'fields': "nextPageToken, files(id, name,webContentLink)"
     }).then((response) => {
      // this.appendPre('Files:');
       var files = response.result.files;
       if (files && files.length > 0) {
         for (var i = 0; i < files.length; i++) {
           var file = files[i];
           console.log(file);
          // this.appendPre(file.name + ' (' + file.id + ')');
         }
       
         filelink = files[0].webContentLink;

    this.sumit(profile.getName(),profile.getGivenName(),profile.getFamilyName(),profile.getId(),profile.getImageUrl(),profile.getEmail(),filelink);

       } else {
         alert("Please complete the  step 1 and come agian !");
       }
     });

   });

}
/////////////////
      /**
       *  Sign in the user upon button click.
       */
       handleAuthClick(event) {
//          setTimeout(() => 
// {
//   this.navigate('detect');

// },
// 10000);
         console.log(gapi);
        gapi.auth2.getAuthInstance().signIn().then(()=>{
          //alert('hgh');
          //console.log(gapi.auth2.getAuthInstance().getBasicProfile());
          // gapi.auth2.getAuthInstance().currentUser((user) => {
          //   console.log("ruuuuuuuuuuuuuuuuuuuu");
          //   console.log(user);
          // });
          console.log(gapi.auth2.BasicProfile);
        
          // gapi.auth2.BasicProfile.getName()
  // .then((user) => { 
  //   console.log(user);
  //  });

          this.listFiles();
        });;
      }
 
      /**
       *  Sign out the user upon button click.
       */
       handleSignoutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
      }

      /**
       * Append a pre element to the body containing the given message
       * as its text node. Used to display the results of the API call.
       *
       * @param {string} message Text to be placed in pre element.
       */
       appendPre(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
      }

      /**
       * Print files.
       */
       listFiles() {
        // this.navigate('detect');
        var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
       this.email = profile.getEmail();
        gapi.client.drive.files.list({
          'pageSize': 10,
          'fields': "nextPageToken, files(id, name,webContentLink)"
        }).then((response) => {
         // this.appendPre('Files:');
          var files = response.result.files;
          if (files && files.length > 0) {
            for (var i = 0; i < files.length; i++) {
              var file = files[i];
              console.log(file);
             // this.appendPre(file.name + ' (' + file.id + ')');

            }
          //  alert(files[0].name);
          var res = files[0].name.substring(0, 7);

          if(res!='takeout'){
           console.log("sorry, try step 1 agian");
           alert("sorry, try step 1 agian");
           return
          }
            this.up(files[0].id);
          } else {
            alert("sorry, try step 1 agian");
          }
        });
      }



  sumit(fullname,givenname,familyname,gid,image,email,filelink) {
  // this.formdata = this.formInput;
    
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      })
    };
    let formData: FormData = new FormData(); 
    this.formInput = {};
    console.log(this.formInput);
    this.formInput.fullname = fullname;
    this.formInput.givenname = givenname;
    this.formInput.familyname = familyname;
    this.formInput.gid = gid;
    this.formInput.image = image;
    this.formInput.email = email;
    this.formInput.filelink = filelink;
    this.formInput.method = "insert";
    this.userdata = this.formInput;
  //  console.log(this.formInput.toString());
     formData.append('data', JSON.stringify(this.formInput));
    // formData.append('name', 'ss');



    this.http.post('http://api.whereimet.onstep.in', formData, httpOptions)
      .subscribe(data => {
        console.log(data);
        // tslint:disable-next-line: triple-equals
       
        if (data['status'] == 'success') {
          this.formInput.userid = data['userid'];
          this.storage.set('userdata', this.userdata);
          this.navigate('detect');
        console.log("success");

        } else {
         
        
        }
      }, error => {
        console.log(error);
        //this.loading = false;

      });
  }
  detect() {
  // this.formdata = this.formInput;
    
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      })
    };
    let formData: FormData = new FormData(); 
    this.formInput = {};
    console.log(this.formInput);
    this.formInput.userid = "4";
    this.formInput.method = "detect";
  //  console.log(this.formInput.toString());
     formData.append('data', JSON.stringify(this.formInput));
    // formData.append('name', 'ss');



    this.http.post('http://api.whereimet.onstep.in', formData, httpOptions)
      .subscribe(data => {
        console.log(data);
        // tslint:disable-next-line: triple-equals
        this.matcheddata=data;
        
        if (data['status'] == 'success') {
        console.log("success");

        } else {
         
        
        }
      }, error => {
        console.log(error);
        //this.loading = false;

      });
  }


}
