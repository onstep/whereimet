import {
  FivCollapsableModule,
  FivRouterItemModule,
  FivExpandableModule
} from '@fivethree/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LottieAnimationViewModule } from 'ng-lottie';
import { UtilService } from '@services/util.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { IntroPage } from './mypages/intro/intro.page';
import { WelcomePage } from './mypages/welcome/welcome.page';
import { MdoButtonModule } from '@ctrl/ngx-github-buttons';
import { ComponentsModule } from './components/components.module';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [AppComponent,IntroPage,WelcomePage],
  entryComponents: [IntroPage,WelcomePage],
  // declarations: [AppComponent,IntroPage,WelcomePage],
  // entryComponents: [IntroPage,WelcomePage],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FivCollapsableModule,
    FivRouterItemModule,
    FivExpandableModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    MdoButtonModule,
    ComponentsModule,
    MarkdownModule.forRoot({ loader: HttpClient }),
    LottieAnimationViewModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    UtilService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
